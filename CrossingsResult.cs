﻿namespace PipeCrosser
{
    public record struct CrossingsResult(Crossing[] crossings, int? dummyPipeIx)
    {
        public static implicit operator (Crossing[] crossings, int? dummyPipeIx)(CrossingsResult value) => (value.crossings, value.dummyPipeIx);
        public static implicit operator CrossingsResult((Crossing[] crossings, int? dummyPipeIx) value) => new CrossingsResult(value.crossings, value.dummyPipeIx);
    }
}