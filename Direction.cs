﻿namespace PipeCrosser
{
    public enum Direction
    {
        Up,
        Right,
        Down,
        Left
    }
}