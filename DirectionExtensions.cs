﻿namespace PipeCrosser
{
    public static class DirectionExtensions
    {
        public static Direction Cw(this Direction dir) => (Direction) (((int) dir + 1) % 4);
        public static Direction Ccw(this Direction dir) => (Direction) (((int) dir + 3) % 4);
        public static Direction UTurn(this Direction dir) => (Direction) ((int) dir ^ 2);
        public static int Dx(this Direction dir) => dir switch { Direction.Right => 1, Direction.Left => -1, _ => 0 };
        public static int Dy(this Direction dir) => dir switch { Direction.Up => -1, Direction.Down => 1, _ => 0 };
        public static bool IsHorizontal(this Direction dir) => ((int) dir & 1) != 0;
    }
}