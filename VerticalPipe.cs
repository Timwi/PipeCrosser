﻿namespace PipeCrosser
{
    struct VerticalPipe
    {
        public int Pipe { get; private set; }
        public VerticalPipe(int pipe) { Pipe = pipe; }
    }
}