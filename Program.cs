﻿using PipeCrosser;

var word = "BAGELS";
var rnd = new Random();

Console.WriteLine("Finding crossings...");
var crossingsResult = PipeAlgorithm.FindCrossings(word, rnd).First();
//ClassifyJson.SerializeToFile(crossingsResult, @"D:\temp\gnubiecrossings.json");
//Console.WriteLine(crossingsResult.crossings.JoinString("\n"));
//Debugger.Break();
//var crossingsResult = ClassifyJson.DeserializeFile<CrossingsResult>(@"D:\temp\blananas2crossings.json");

var (crossings, dummyPipeIx) = crossingsResult;

Console.WriteLine("Generating graphics...");
File.WriteAllText(@"D:\temp\temp.svg", PipeAlgorithm.MakeSvg(crossings, dummyPipeIx, rnd));


//var grid = ClassifyJson.DeserializeFile<PipeGrid>(@"D:\temp\brawlboxgrid.json");
//File.WriteAllText(@"D:\temp\temp.svg", grid.MakeSvg("red,orange,yellow,green,cyan,blue,purple,pink,silver".Split(',')));

