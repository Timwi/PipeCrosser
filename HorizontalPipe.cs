﻿namespace PipeCrosser
{
    struct HorizontalPipe
    {
        public int Pipe { get; private set; }
        public HorizontalPipe(int pipe) { Pipe = pipe; }
    }
}