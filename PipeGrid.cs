﻿using System.Diagnostics;
using System.Numerics;
using RT.Util;
using RT.Util.ExtensionMethods;

namespace PipeCrosser
{
    class PipeGrid
    {
        private object[] _grid = Array.Empty<object>();
        private int _w = 0;
        private int _h = 0;

        public int Width => _w;
        public int Height => _h;

        public int? GetPipe(int cell, Direction dir) =>
            dir.IsHorizontal() && _grid[cell] is HorizontalPipe hp ? hp.Pipe :
            !dir.IsHorizontal() && _grid[cell] is VerticalPipe vp ? vp.Pipe :
            _grid[cell] is PipeBend pb && (pb.Direction == dir || pb.Direction.Cw() == dir) ? pb.Pipe :
            _grid[cell] is Crossing c && c.Done[(int) dir] ? dir.IsHorizontal() == c.Direction.IsHorizontal() ? c.LeftPipe : c.RightPipe : null;

        public int? IsPipeStart(int cell, Direction dir)
        {
            var x = cell % _w;
            var y = cell / _w;

            if (_grid[cell] == null || (dir.IsHorizontal() && _grid[cell] is VerticalPipe) || (!dir.IsHorizontal() && _grid[cell] is HorizontalPipe))
                return null;
            if (_grid[cell] is PipeBend pbs && pbs.Direction != dir.Cw() && pbs.Direction != dir.UTurn())
                return null;

            while (!OutOfRange(x, y))
            {
                if (_grid[x + _w * y] is Crossing cr)
                    return cr.Direction == dir ? cr.LeftPipe : cr.Direction == dir.Ccw() ? cr.RightPipe : null;
                FollowPipe(ref x, ref y, ref dir);
            }
            throw new InvalidOperationException($"The pipe starting at cell {cell} has no crossings.");
        }

        private void FollowPipe(ref int x, ref int y, ref Direction dir)
        {
            if (_grid[x + _w * y] is PipeBend pb)
            {
                if (pb.Direction.UTurn() == dir)
                    dir = dir.Ccw();
                else if (pb.Direction.Ccw() == dir)
                    dir = dir.Cw();
                else
                    throw new InvalidOperationException($"Invalid pipe bend.");
            }

            x += dir.Dx();
            y += dir.Dy();
        }

        public void Expand()
        {
            var newW = 2 * _w + 1;
            var newH = 2 * _h + 1;
            _grid = Ut.NewArray(newW * newH, ix =>
            {
                int? p;
                var x = ix % newW;
                var y = ix / newW;
                return x % 2 == 0
                    ? y % 2 != 0
                        ? (((x < newW - 1 && (p = GetPipe(x / 2 + _w * (y / 2), Direction.Left)) != null) || (x > 0 && (p = GetPipe(x / 2 - 1 + _w * (y / 2), Direction.Right)) != null)) ? new HorizontalPipe(p.Value) : null)
                        : null
                    : y % 2 == 0
                        ? (((y < newH - 1 && (p = GetPipe(x / 2 + _w * (y / 2), Direction.Up)) != null) || (y > 0 && (p = GetPipe(x / 2 + _w * (y / 2 - 1), Direction.Down)) != null)) ? new VerticalPipe(p.Value) : null)
                        : _grid[(x / 2) + _w * (y / 2)];
            });
            _w = newW;
            _h = newH;
        }

        public void Simplify()
        {
            // Remove unnecessary columns and rows
            for (var col = _w - 1; col >= 0; col--)
                if (Enumerable.Range(0, _h).All(y => _grid[col + _w * y] is null or HorizontalPipe))
                    _grid = Ut.NewArray((--_w) * _h, ix => _grid[ix % _w + (ix % _w >= col ? 1 : 0) + (_w + 1) * (ix / _w)]);
            for (var row = _h - 1; row >= 0; row--)
                if (Enumerable.Range(0, _w).All(x => _grid[x + _w * row] is null or VerticalPipe))
                    _grid = Ut.NewArray(_w * (--_h), ix => _grid[ix % _w + _w * (ix / _w + (ix / _w >= row ? 1 : 0))]);
        }

        private void AddPathToGrid(int pipe, (int x, int y)[] path, Direction lastExitDir)
        {
            for (var i = 1; i < path.Length; i++)
            {
                var enterDir = GetDir(path[i - 1], path[i]);
                var exitDir = i == path.Length - 1 ? lastExitDir : GetDir(path[i], path[i + 1]);
                _grid[path[i].x + _w * path[i].y] = enterDir == exitDir ? enterDir.IsHorizontal() ? new HorizontalPipe(pipe) : new VerticalPipe(pipe) :
                    enterDir.Cw() == exitDir ? new PipeBend(exitDir, pipe) :
                    enterDir.Ccw() == exitDir ? new PipeBend(enterDir.UTurn(), pipe) : throw new InvalidOperationException();
            }
        }

        public bool Contains(Crossing crossing) => _grid.Contains(crossing);

        private int FindCell(Crossing crossing)
        {
            var ix = _grid.IndexOf(crossing);
            return ix == -1 ? throw new InvalidOperationException($"The crossing {crossing} is not in the grid.") : ix;
        }

        public void AddPathToEdge(int pipe, int startX, int startY, Direction startDir, Crossing[] crossingsToAdd)
        {
            var (cells, exitDir) = FindPath(startX, startY, startDir, OutOfRange);
            AddPathToGrid(pipe, cells, exitDir);
            AddCrossings(pipe, cells, crossingsToAdd, exitDir);
        }

        public void AddPathToEdge(int pipe, Crossing crossing, Crossing[] crossingsToAdd, bool reverse = false)
        {
            if (!crossing.Contains(pipe))
                throw new ArgumentException($"The specified crossing, {crossing}, does not contain pipe {pipe}.", nameof(pipe));
            var startCell = FindCell(crossing);
            var initialDir = crossing.GetDirection(pipe).Apply(d => reverse ? d.UTurn() : d);
            var (cells, exitDir) = FindPath(
                startX: startCell % _w,
                startY: startCell / _w,
                initialDir: initialDir,
                isEnd: OutOfRange);
            if (reverse)
            {
                var (lx, ly) = cells.Last();
                lx += exitDir.Dx(); ly += exitDir.Dy();
                AddPath(pipe, lx, ly, exitDir.UTurn(), crossing, crossingsToAdd);
            }
            else
            {
                AddPathToGrid(pipe, cells, exitDir);
                crossing.Done[(int) initialDir] = true;
                AddCrossings(pipe, cells, crossingsToAdd, exitDir);
            }
        }

        private bool InRange(int x, int y) => x >= 0 && x < _w && y >= 0 && y < _h;
        private bool OutOfRange(int x, int y) => x < 0 || x >= _w || y < 0 || y >= _h;

        public void AddPath(int pipe, Crossing startCrossing, Crossing endCrossing, Crossing[] crossingsToAdd)
        {
            var startCrossingCell = FindCell(startCrossing);
            var startDir = startCrossing.GetDirection(pipe);
            AddPath(pipe, startCrossingCell % _w, startCrossingCell / _w, startDir, endCrossing, crossingsToAdd);
            startCrossing.Done[(int) startDir] = true;
        }

        public void AddPath(int pipe, int startX, int startY, Direction startDir, Crossing endCrossing, Crossing[] crossingsToAdd)
        {
            var endCrossingCell = FindCell(endCrossing);
            var endDir = endCrossing.GetDirection(pipe);
            var endX = endCrossingCell % _w;
            var endY = endCrossingCell / _w;
            (int x, int y, Direction dir)? alternateEnd = null;
            var (cells, exitDir) = FindPath(startX, startY, startDir, (cx, cy) =>
            {
                if (cx == endX && cy == endY)
                    return true;
                if (OutOfRange(cx, cy) || _grid[cx + _w * cy] is not HorizontalPipe and not VerticalPipe)
                    return false;
                var dir = endDir.Cw();
                for (int x = endX + dir.Dx(), y = endY + dir.Dy(); InRange(x, y) && _grid[x + _w * y] is not Crossing; FollowPipe(ref x, ref y, ref dir))
                    if (x == cx && y == cy)
                    {
                        alternateEnd = (x, y, dir.Ccw());
                        return true;
                    }
                dir = endDir.Ccw();
                for (int x = endX + dir.Dx(), y = endY + dir.Dy(); InRange(x, y) && _grid[x + _w * y] is not Crossing; FollowPipe(ref x, ref y, ref dir))
                    if (x == cx && y == cy)
                    {
                        alternateEnd = (x, y, dir.Cw());
                        return true;
                    }
                return false;
            });

            if (alternateEnd != null)
            {
                var otherPipe = endCrossing.LeftPipe == pipe ? endCrossing.RightPipe : endCrossing.LeftPipe;
                _grid[endCrossingCell] = endCrossing.GetDirection(otherPipe).IsHorizontal() ? new HorizontalPipe(otherPipe) : new VerticalPipe(otherPipe);
                while (endCrossing.GetDirection(pipe) != alternateEnd.Value.dir)
                    endCrossing.Cw();
                _grid[alternateEnd.Value.x + _w * alternateEnd.Value.y] = endCrossing;
            }

            AddPathToGrid(pipe, cells, exitDir);
            endCrossing.Done[(int) exitDir.UTurn()] = true;
            AddCrossings(pipe, cells, crossingsToAdd, exitDir);
        }

        private void AddCrossings(int pipe, (int x, int y)[] path, Crossing[] crossingsToAdd, Direction finalDir)
        {
            var ctaIx = 0;
            void LayCrossing(int cx, int cy, Direction dir)
            {
                var cta = crossingsToAdd[ctaIx++];
                if (cta.LeftPipe == pipe)
                    cta.Direction = dir;
                else if (cta.RightPipe == pipe)
                    cta.Direction = dir.Ccw();
                else
                    throw new InvalidOperationException();
                cta.Done[(int) dir] = true;
                cta.Done[(int) dir.UTurn()] = true;
                _grid[cx + _w * cy] = cta;
            }

            for (var i = 1; i < path.Length && ctaIx < crossingsToAdd.Length; i++)
                if (_grid[path[i].x + _w * path[i].y] is HorizontalPipe or VerticalPipe)
                    //if ((path[i].x % 2 != 0 || path[i].y % 2 != 0) && _grid[path[i].x + _w * path[i].y] is HorizontalPipe or VerticalPipe)
                    LayCrossing(path[i].x, path[i].y, GetDir(path[i - 1], path[i]));

            var (x, y) = path.Last();
            while (ctaIx < crossingsToAdd.Length)
            {
                switch (finalDir)
                {
                    case Direction.Up: InsertRow(y); break;
                    case Direction.Right: InsertColumn(++x); break;
                    case Direction.Down: InsertRow(++y); break;
                    case Direction.Left: InsertColumn(x); break;
                    default: throw new InvalidOperationException();
                }
                LayCrossing(x, y, finalDir);
            }
        }

        private void InsertColumn(int x)
        {
            var newW = _w + 1;
            _grid = Ut.NewArray(newW * _h, ix => ix % newW == x
                    ? x > 0 && GetPipe(x - 1 + _w * (ix / newW), Direction.Right) is int pipe ? new HorizontalPipe(pipe) :
                        x == 0 && GetPipe(_w * (ix / newW), Direction.Left) is int pipe2 ? new HorizontalPipe(pipe2) : null
                    : _grid[ix % newW - (ix % newW >= x ? 1 : 0) + _w * (ix / newW)]);
            _w = newW;
        }

        private void InsertRow(int y)
        {
            var newH = _h + 1;
            _grid = Ut.NewArray(_w * newH, ix => ix / _w == y
                    ? y > 0 && GetPipe(ix % _w + _w * (y - 1), Direction.Down) is int pipe ? new VerticalPipe(pipe) :
                        y == 0 && GetPipe(ix % _w, Direction.Up) is int pipe2 ? new VerticalPipe(pipe2) : null
                    : _grid[ix % _w + _w * (ix / _w - (ix / _w >= y ? 1 : 0))]);
            _h = newH;
        }

        private static Direction GetDir((int x, int y) start, (int x, int y) end) => start.x == end.x
            ? start.y < end.y
                ? Direction.Down
                : Direction.Up
            : start.y == end.y
                ? start.x < end.x
                    ? Direction.Right
                    : Direction.Left
                : throw new InvalidOperationException($"The cells {start} and {end} are not orthogonal from one another.");

        private ((int x, int y)[] cells, Direction exitDir) FindPath(int startX, int startY, Direction initialDir, Func<int, int, bool> isEnd)
        {
            var q = new Queue<((int x, int y)[] pathSoFar, Direction dir)>();
            q.Enqueue((new (int x, int y)[] { (startX, startY) }, initialDir));
            while (q.Count > 0)
            {
                var (pathSoFar, dir) = q.Dequeue();
                var dx = dir.Dx();
                var dy = dir.Dy();
                var (x, y) = pathSoFar.Last();
                var candidates = new List<(int x, int y)>();
                x += dx;
                y += dy;
                while (InRange(x, y) && _grid[x + _w * y] == null && !pathSoFar.Contains((x, y)))
                {
                    candidates.Add((x, y));
                    x += dx;
                    y += dy;
                }
                if (isEnd(x, y))
                    return (pathSoFar.Concat(candidates).ToArray(), dir);

                for (var i = 1; i <= candidates.Count; i++)
                {
                    var newPath = pathSoFar.Concat(candidates.Take(i)).ToArray();
                    q.Enqueue((newPath, dir.Cw()));
                    q.Enqueue((newPath, dir.Ccw()));
                }
            }
            throw new InvalidOperationException($"There is no valid path from ({startX},{startY}).");
        }

        public string MakeSvg(string[] colors) => $@"<svg xmlns='http://www.w3.org/2000/svg' viewBox='-1 -1 {_w + 2} {_h + 2}'>
                {Enumerable.Range(0, _h).SelectMany(y => Enumerable.Range(0, _w).Select(x =>
                                                                   _grid[x + _w * y] is HorizontalPipe hp ? $"<path d='M{x} {y + .5}h1' stroke='{colors[hp.Pipe]}' stroke-width='.5' />" :
                                                                   _grid[x + _w * y] is VerticalPipe vp ? $"<path d='M{x + .5} {y}v1' stroke='{colors[vp.Pipe]}' stroke-width='.5' />" :
                                                                   _grid[x + _w * y] is PipeBend pb ? $"<path d='M0 -.5v.5h.5' transform='translate({x + .5}, {y + .5}) rotate({90 * (int) pb.Direction})' stroke='{colors[pb.Pipe]}' stroke-width='.5' />" :
                                                                   _grid[x + _w * y] is Crossing c ? $@"
    <g transform='translate({x + .5}, {y + .5}) rotate({90 * (int) c.Direction})' stroke-width='.5'>
        {new[] { $"<path d='M0 -.5v1' stroke='{colors[c.LeftPipe]}' />", $"<path d='M-.5 0h1' stroke='{colors[c.RightPipe]}' />" }.Apply(a => c.Over ? a.Reverse() : a).JoinString()}
        {(false ? $"<path transform='translate(-.5, -.5)' d='M.5 .2 .5 .8M.4 .3 .5 .2 .6 .3' fill='none' stroke='black' stroke-width='.05' />" : "")}
        {(false ? $"<path transform='rotate(90) translate(-.5, -.5)' d='M.5 .2 .5 .8M.4 .3 .5 .2 .6 .3' fill='none' stroke='black' stroke-width='.05' />" : "")}
    </g>
    {(false ? c.Done.Select((b, dir) => b ? $"<circle cx='{x + .5 + dir switch { 1 => .3, 3 => -.3, _ => 0 }}' cy='{y + .5 + dir switch { 0 => -.3, 2 => .3, _ => 0 }}' r='.1' />" : "").JoinString() : "")}
" :
                                                                   "")).JoinString()}
                {(true ? Enumerable.Range(0, _w).Select(x => GetPipe(x, Direction.Up) is int pipe ? $"<path d='M{x + .5} 0v-.25' fill='none' stroke='{colors[pipe]}' stroke-width='{(IsPipeStart(x, Direction.Down) != null ? ".75" : ".5")}' />" : "").JoinString() : "")}
                {(true ? Enumerable.Range(0, _h).Select(y => GetPipe(_w - 1 + _w * y, Direction.Right) is int pipe ? $"<path d='M{_w} {y + .5}h.25' fill='none' stroke='{colors[pipe]}' stroke-width='{(IsPipeStart(_w - 1 + _w * y, Direction.Left) != null ? ".75" : ".5")}' />" : "").JoinString() : "")}
                {(true ? Enumerable.Range(0, _w).Select(x => GetPipe(x + _w * (_h - 1), Direction.Down) is int pipe ? $"<path d='M{x + .5} {_h}v.25' fill='none' stroke='{colors[pipe]}' stroke-width='{(IsPipeStart(x + _w * (_h - 1), Direction.Up) != null ? ".75" : ".5")}' />" : "").JoinString() : "")}
                {(true ? Enumerable.Range(0, _h).Select(y => GetPipe(_w * y, Direction.Left) is int pipe ? $"<path d='M0 {y + .5}h-.25' fill='none' stroke='{colors[pipe]}' stroke-width='{(IsPipeStart(_w * y, Direction.Right) != null ? ".75" : ".5")}' />" : "").JoinString() : "")}
                <path fill='none' stroke='black' stroke-opacity='.5' stroke-width='.02' d='{Enumerable.Range(0, _w + 1).Select(x => $"M{x} 0v{_h}").JoinString()}{Enumerable.Range(0, _h + 1).Select(y => $"M0 {y}h{_w}").JoinString()}' />
            </svg>";

        public int? FindPipeWithUnfinishedCrossing()
        {
            int p;
            for (var i = 0; i < _grid.Length; i++)
                if (_grid[i] is Crossing c && (p = c.Done.IndexOf(false)) != -1)
                    return ((Direction) p).IsHorizontal() == c.Direction.IsHorizontal() ? c.LeftPipe : c.RightPipe;
            return null;
        }
    }
}