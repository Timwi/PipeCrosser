﻿using RT.Serialization;
using RT.Util;
using RT.Util.ExtensionMethods;

namespace PipeCrosser
{
    public static class PipeAlgorithm
    {
        public static readonly (string morse, string ch)[] Morse =
            @".-|-...|-.-.|-..|.|..-.|--.|....|..|.---|-.-|.-..|--|-.|---|.--.|--.-|.-.|...|-|..-|...-|.--|-..-|-.--|--..".Split('|').Select((str, ix) => (str, ((char) ('A' + ix)).ToString()))
                .Concat(@"-----|.----|..---|...--|....-|.....|-....|--...|---..|----.".Split('|').Select((str, ix) => (str, ((char) ('0' + ix)).ToString())))
                .Concat(@"Ä=.-.-|Á=.--.-|Ch=----|É=..-..|Ñ=--.--|Ö=---.|Ü=..--".Split('|').Select(t => t.Split('=')).Select(arr => (arr[1], arr[0])))
                .Concat(@".>.-.-.-|,>--..--|:>---...|?>..--..|'>.----.|->-....-|/>-..-.|(>-.--.|)>-.--.-|"">.-..-.|@>.--.-.|=>-...-|!>-.-.--".Split('|').Select(t => t.Split('>')).Select(arr => (arr[1], arr[0])))
                .ToArray();

        public static readonly Dictionary<string, string> MorseFromCh = Morse.ToDictionary(m => m.ch, m => m.morse);
        public static readonly Dictionary<string, string> ChFromMorse = Morse.ToDictionary(m => m.morse, m => m.ch);

        abstract class MorseThread
        {
            public int Index { get; private set; }
            public MorseThread(int index) { Index = index; }
            public abstract int Length { get; }
            public abstract bool CanRemove(bool dash);
            public abstract MorseThread Remove(bool dash);
        }
        sealed class MorseThreadSpecific : MorseThread
        {
            public bool[] Morse { get; private set; }
            public MorseThreadSpecific(bool[] morse, int index) : base(index) { Morse = morse; }
            public override int Length => Morse.Length;
            public override bool CanRemove(bool dash) => Morse.Length > 0 && Morse[0] == dash;
            public override MorseThread Remove(bool dash) => Morse.Length == 0 || Morse[0] != dash ? throw new InvalidOperationException() : new MorseThreadSpecific(Morse.Subarray(1), Index);
            public override string ToString() => Morse.Select(b => b ? "−" : "•").JoinString();
        }
        sealed class MorseThreadNonspecific : MorseThread
        {
            public int AllowedDots { get; private set; }
            public int AllowedDashes { get; private set; }
            public MorseThreadNonspecific(int dots, int dashes, int index) : base(index) { AllowedDots = dots; AllowedDashes = dashes; }
            public override int Length => AllowedDots + AllowedDashes;
            public override bool CanRemove(bool dash) => (dash ? AllowedDashes : AllowedDots) > 0;
            public override MorseThread Remove(bool dash) => ((dash ? AllowedDashes : AllowedDots) == 0) ? throw new InvalidOperationException() : new MorseThreadNonspecific(dash ? AllowedDots : AllowedDots - 1, dash ? AllowedDashes - 1 : AllowedDashes, Index);
            public override string ToString() => $"{AllowedDots} × • / {AllowedDashes} × −";
        }

        public static IEnumerable<CrossingsResult> FindCrossings(string word, Random rnd)
        {
            var morses = word.Select((ch, ix) => new MorseThreadSpecific(MorseFromCh[ch.ToString()].Select(c => c == '-').ToArray(), ix)).ToArray();
            var morsesTmp = word.Select((ch, ix) => MorseFromCh[ch.ToString()]).ToArray();
            var dots = morses.Sum(m => m.Morse.Count(b => !b));
            var dashes = morses.Sum(m => m.Morse.Count(b => b));

            return Enumerable.Range(0, int.MaxValue).SelectMany(numDummy =>
            {
                if (dots == dashes && numDummy == 0)
                    return recurse(Array.Empty<Crossing>(), morses.SelectMany(mt => new[] { mt, null }).ToArray()).Select(result => new CrossingsResult(result, (int?) null));

                return Enumerable.Range(1, morses.Length).Apply(r => rnd == null ? r.Reverse() : r.ToArray().Shuffle()).SelectMany(dummyPipeIx =>
                {
                    var dummyPipe = new MorseThreadNonspecific((dots > dashes ? 0 : dashes - dots) + numDummy, (dots > dashes ? dots - dashes : 0) + numDummy, dummyPipeIx);
                    var newMorses = word.Select((ch, ix) => (MorseThread) new MorseThreadSpecific(MorseFromCh[ch.ToString()].Select(c => c == '-').ToArray(), ix >= dummyPipeIx ? ix + 1 : ix)).ToArray()
                        .Insert(dummyPipeIx, dummyPipe).SelectMany(mt => new[] { mt, null }).ToArray();
                    return recurse(Array.Empty<Crossing>(), newMorses).Select(result => new CrossingsResult(result, dummyPipeIx));
                });
            });
        }

        private static IEnumerable<Crossing[]> recurse(Crossing[] crossingsSofar, MorseThread[] remaining)
        {
            // Cannot continue if there are no gaps left
            if (remaining.All(r => r != null))
                yield break;

            // Small optimization
            for (var i = 1; i < remaining.Length - 1; i++)
            {
                if (remaining[i - 1] is MorseThread tLeft && tLeft.Length == 0 &&
                    remaining[i] is MorseThread tMiddle && tMiddle.Length > 0 &&
                    remaining[i + 1] is MorseThread tRight && tRight.Length == 0)
                    yield break;
            }

            var count = remaining.Where(mt => mt != null && mt.Length > 0).Take(2).Count();
            if (count == 0)
            {
                yield return crossingsSofar;
                yield break;
            }
            if (count == 1)
                yield break;

            for (int leftSlot = 0; leftSlot < remaining.Length; leftSlot++)
                if (remaining[leftSlot] is MorseThread leftMorse && leftMorse.Length > 0)
                    for (int rightSlot = (leftSlot + 1) % remaining.Length; rightSlot != leftSlot; rightSlot = (rightSlot + 1) % remaining.Length)
                    {
                        if (remaining[rightSlot] is not MorseThread rightMorse)
                            continue;

                        // We’re allowed to cross over across 0-length slots only if the 0-length slot is adjacent to a gap
                        if (rightMorse.Length == 0
                                && remaining[(rightSlot + remaining.Length - 1) % remaining.Length] != null
                                && remaining[(rightSlot + 1) % remaining.Length] != null)
                            break;

                        if (rightMorse.Length == 0)
                            continue;

                        var leftIsDash = false;
                        secondIteration:

                        if (leftMorse.CanRemove(leftIsDash) && rightMorse.CanRemove(!leftIsDash))
                        {
                            var newCrossingsSofar = crossingsSofar.Append(new Crossing(leftMorse.Index, rightMorse.Index, leftIsDash));
                            var newRemaining = remaining.ToList();
                            newRemaining[leftSlot] = rightMorse.Remove(!leftIsDash);
                            newRemaining[rightSlot] = leftMorse.Remove(leftIsDash);
                            if (leftSlot < rightSlot)
                                newRemaining.RemoveRange(leftSlot + 1, rightSlot - leftSlot - 1);
                            else
                            {
                                newRemaining.RemoveRange(leftSlot + 1, newRemaining.Count - leftSlot - 1);
                                newRemaining.RemoveRange(0, rightSlot);
                            }

                            foreach (var result in recurse(newCrossingsSofar, newRemaining.ToArray()))
                                yield return result;
                        }
                        if (!leftIsDash)
                        {
                            leftIsDash = true;
                            goto secondIteration;
                        }
                        break;
                    }
        }

        public static string[] Colors;

        public static string MakeSvg(Crossing[] crossings, int? dummyPipe, Random rnd = null)
        {
            var allPipes = crossings.SelectMany(c => new[] { c.LeftPipe, c.RightPipe }).Distinct().ToList();
            if (rnd != null)
                allPipes.Shuffle(rnd);

            var numColors = dummyPipe != null ? allPipes.Count - 1 : allPipes.Count;
            var colors = Ut.NewArray(numColors, ix => $"hsl({360d * ix / numColors}, 85%, 50%)");
            if (dummyPipe is int dIx)
                colors = colors.Insert(dIx, "hsl(0, 0%, 85%)");
            Colors = colors;

            var grid = new PipeGrid();

            while (allPipes.Count > 0)
            {
                // Find a pipe that already has a crossing in the grid
                if (grid.FindPipeWithUnfinishedCrossing() is int pipe)
                {
                    var relevantCrossings = crossings.Where(c => c.Contains(pipe)).ToArray();
                    var alreadyPresent = relevantCrossings.Select(grid.Contains).ToArray();
                    for (int left = -1, right = Array.IndexOf(alreadyPresent, true); true; left = right, right = Array.IndexOf(alreadyPresent, true, right + 1))
                    {
                        var crossingsToAdd = relevantCrossings.Subarray(left + 1, (right == -1 ? relevantCrossings.Length : right) - left - 1);
                        grid.Expand();
                        if (left == -1)
                            // Find a connection from the earliest crossing (‘right’) to the edge, then reverse it
                            grid.AddPathToEdge(pipe, relevantCrossings[right], crossingsToAdd, reverse: true);
                        else if (right == -1)
                            // Find a connection from the last crossing (‘left’) to the edge
                            grid.AddPathToEdge(pipe, relevantCrossings[left], crossingsToAdd);
                        else
                        {
                            // Find a connection from ‘left’ to ‘right’
                            grid.AddPath(pipe, relevantCrossings[left], relevantCrossings[right], crossingsToAdd);
                        }
                        grid.Simplify();
                        if (right == -1)
                            break;
                    }
                    allPipes.Remove(pipe);
                }
                else
                {
                    pipe = allPipes[0];
                    var relevantCrossings = crossings.Where(c => c.Contains(pipe)).ToArray();

                    // Brand new pipe! We need to insert it at the grid’s edge *after* the latest (in clockwise order) start of a pipe with a lower index.
                    grid.Expand();
                    grid.Expand();
                    var edgeCells = Enumerable.Range(0, grid.Width).Select(x => (x, y: 0, dir: Direction.Down))
                        .Concat(Enumerable.Range(0, grid.Height).Select(y => (x: grid.Width - 1, y, dir: Direction.Left)))
                        .Concat(Enumerable.Range(0, grid.Width).Select(x => (x: grid.Width - 1 - x, y: grid.Height - 1, dir: Direction.Up)))
                        .Concat(Enumerable.Range(0, grid.Height).Select(y => (x: 0, y: grid.Height - 1 - y, dir: Direction.Right)))
                        .ToArray();

                    var (startX, startY, startDir) = (0, -1, Direction.Down);

                    var candidateIx = edgeCells.LastIndexOf(tup => grid.IsPipeStart(tup.x + grid.Width * tup.y, tup.dir) is int p && p < pipe);
                    if (candidateIx >= 0)
                    {
                        (startX, startY, startDir) = edgeCells[candidateIx];
                        startX += startDir.Ccw().Dx(); startY += startDir.Ccw().Dy();
                        startX += startDir.UTurn().Dx(); startY += startDir.UTurn().Dy();
                    }
                    else
                    {
                        candidateIx = edgeCells.IndexOf(tup => grid.IsPipeStart(tup.x + grid.Width * tup.y, tup.dir) is int p && p > pipe);
                        if (candidateIx >= 0)
                        {
                            (startX, startY, startDir) = edgeCells[candidateIx];
                            startX += startDir.Cw().Dx(); startY += startDir.Cw().Dy();
                            startX += startDir.UTurn().Dx(); startY += startDir.UTurn().Dy();
                        }
                    }

                    grid.AddPathToEdge(pipe, startX, startY, startDir, relevantCrossings);
                    grid.Simplify();
                    allPipes.RemoveAt(0);
                }
            }

            return grid.MakeSvg(colors);
        }
    }
}