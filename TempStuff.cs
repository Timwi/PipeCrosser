﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RT.Util.ExtensionMethods;

namespace PipeCrosser
{
    public class TempStuff
    {
        private readonly int _w;
        private readonly int _h;

        public int Width => _w;
        public int Height => _h;

        public TempStuff(int w, int h) { _w = w; _h = h; }

        private bool isEdge((int x, int y) tup) => isEdge(tup.x, tup.y);
        private bool isEdge(int x, int y) => x == 0 || x == _w - 1 || y == 0 || y == _h - 1;

        public IEnumerable<(int x, int y)[]> GetPaths((int x, int y)[] sofar)
        {
            if (sofar.Length > 1 && isEdge(sofar.Last()))
                yield return sofar;

            var (x, y) = sofar.Last();
            // Left
            if (x > 0 && !sofar.Contains((x - 1, y)))
                foreach (var path in GetPaths(sofar.Append((x - 1, y))))
                    yield return path;
            // Right
            if (x < _w - 1 && !sofar.Contains((x + 1, y)))
                foreach (var path in GetPaths(sofar.Append((x + 1, y))))
                    yield return path;
            // Up
            if (y > 0 && !sofar.Contains((x, y - 1)))
                foreach (var path in GetPaths(sofar.Append((x, y - 1))))
                    yield return path;
            // Down
            if (y < _h - 1 && !sofar.Contains((x, y + 1)))
                foreach (var path in GetPaths(sofar.Append((x, y + 1))))
                    yield return path;
        }

        public IEnumerable<(int x, int y)[]> GetPaths()
        {
            // Top edge
            for (var x = 0; x < _w - 1; x++)
                foreach (var path in GetPaths(new[] { (x, 0) }))
                    yield return path;
            // Right edge
            for (var y = 0; y < _h - 1; y++)
                foreach (var path in GetPaths(new[] { (_w - 1, y) }))
                    yield return path;
            // Bottom edge
            for (var x = _w - 1; x > 0; x--)
                foreach (var path in GetPaths(new[] { (x, _h - 1) }))
                    yield return path;
            // Left edge
            for (var y = _h - 1; y > 0; y--)
                foreach (var path in GetPaths(new[] { (0, y) }))
                    yield return path;
        }

        public static void Foo(int w, int h)
        {
            var n = new TempStuff(w, h);
            Console.WriteLine(n.GetPaths().Count());
            //foreach (var path in n.GetPaths())
            //    Console.WriteLine(path.JoinString(" → "));
        }
    }
}
