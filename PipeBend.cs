﻿namespace PipeCrosser
{
    struct PipeBend
    {
        public Direction Direction { get; private set; }
        public int Pipe { get; private set; }
        public PipeBend(Direction direction, int pipe) { Direction = direction; Pipe = pipe; }
    }
}