﻿namespace PipeCrosser
{
    public class Crossing
    {
        public int LeftPipe;
        public int RightPipe;
        public bool Over;
        public Direction Direction;
        public bool[] Done = new bool[4];

        public Crossing(int leftPipe, int rightPipe, bool over)
        {
            LeftPipe = leftPipe;
            RightPipe = rightPipe;
            Over = over;
        }

        private Crossing() { }  // for Classify

        public override string ToString() => $"{LeftPipe}{(Over ? "↑" : "↓")}{RightPipe}{"↑→↓←"[(int) Direction]}";

        public bool Contains(int pipe) => LeftPipe == pipe || RightPipe == pipe;
        public Direction GetDirection(int pipe) => LeftPipe == pipe ? Direction : RightPipe == pipe ? Direction.Cw() : throw new InvalidOperationException($"Cannot get direction of pipe {pipe} that is not part of this crossing: {this}.");

        public void Cw()
        {
            Direction = Direction.Cw();
            var b = Done[0];
            Done[0] = Done[1];
            Done[1] = Done[2];
            Done[2] = Done[3];
            Done[3] = b;
        }
    }
}